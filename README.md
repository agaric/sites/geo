# GEO.coop README

This project is based on Drutopia, which means it is compatible with
existing and future Drutopia features.  We will be diverging from
Drutopia for special functionality, however.

See [DEPLOYMENT.md](DEPLOYMENT.md) for easy deploy instructions with Ahoy.

## Temporary upgrade notes

There's an error in the update hooks about url alias table not found, but confirmed that all the entries in `old_5ebc14_url_alias` also exist in the new `path_alias` table, so whatever happened there, nothing is lost in the upgrade.

## Getting started

One time:

```
git clone git@gitlab.com:agaric/sites/geo.git
cd geo
```

[Install DDEV](https://ddev.readthedocs.io/en/latest/#installation) for
the following steps to work.

Whenever you want a fresh start:


```
git pull
ddev start
ddev composer update
ddev . drush -y site:install minimal --existing-config
```

Note: If you receive a `Permission denied (publickey).` error it means you need to copy your key into the DDEV container by running `ddev auth ssh`.

## Pull live content down for development

```
drush sql-sync @live @self
drush -y rsync --exclude-paths=css:js:php --verbose @live:%files/ @self:%files
```


## Running the migration in development

Download the D7 files via the Backup and Migrate module.

Put the D7 files in the following location:
```
<Project Root>/d7/sites/default/files/
```
There is a reference to the `sites/default/files` in the database so that structure is mandatory for the migration.

The path in the `upgrade_d7_file.yml` migration file is different because this is executed inside the docker container so in the docker container the <Project Root> is: `/var/www/html/` but no need to re-create those folders in the host machine.

Download a database dump of the D7 site via the Backup and Migrate module.

Create a database called `drupal7`:

Use `ddev describe` to see what the host and port are for your local environment, and use that in the first line here:

```
mysql --host=127.0.0.1 --port=32779 --user=root --password=root --database=db
CREATE DATABASE drupal7;
GRANT ALL ON drupal7.* to 'db'@'%' IDENTIFIED BY 'db';
```

Now `ctrl+c` out of there, and, again substituting the host and port for your own, import your file from whereever you placed and named your db SQL dump:

```
mysql --host=127.0.0.1 --port=32779 --user=root --password=root --database=drupal7 < data/db.mysql
```

(Above per [instructions for creating another database in ddev](https://stackoverflow.com/a/49785024/1943924).)

Once the files are in the correct place and the database has been created we can run the following  to run all the migrations:

```
ddev . drush mim --group="migrate_drupal_7_geo"
```
Note: The migration will partially fail because of missing files. For now, re-run the migration command and it will complete the migration.

## Running the migration on the hosted instance

1. Refresh files and database from live.
   - Obtain the latest from the prior site via [Migrate and Backup](http://geo.coop/admin/config/system/backup_migrate): database, and public files, as two separate downloads. The following commands assume the files saved as `files.tar.gz` and `database.sql.gz`
   - Upload these to the server: `scp *.gz geo_live@drutopia.org:`
   - SSH to server: `ssh geo_live@drutopia.org`
   - Unzip the uploaded database file: `gunzip database.sql.gz`
   - Drop the existing tables: `drop-tables.sh geo_legacy`
   - Reload the uploaded database: `mysql geo_legacy < database.sql`
   - Change to the files folder and untar them: `cd site/d7/sites/default/files/` and then: `tar xzf ~/files.tar.gz` (this will take a couple minutes).
   - The server side is ready! Log off by typing `ctrl+d`
1. Now perform a deployment with drutopia_hosting. This will install the site using the minimal profile and existing config. Set the vars accordingly (drupal_install and drupal_forceinstall must be True), if needed: `ahoy vault-edit`
   - Deploy a new build, as needed: `ahoy deploy-build news`
   - Deploy the site. With a forced re-install, a migration will be performed automatically: `ahoy deploy-site geo_live`

## Post go-live tasks

1. Remove all d7 files: `rm -rf site/d7` and `rm geo_legacy.sql`, etc.
1. Change the site settings in hosting:
   - config_import -> True
   - php_settings_code -> blank
1. Change the news build settings in hosting:
   - remove custom build commands (i.e. sed)
1. At this point, the deploy should be typical (deploy-build/deploy-site as normal)
