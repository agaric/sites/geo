drupal 8 internet archive link module fix broken links
drupal fix broken external links
automatically fix broken links with links to internet archive

D7 only and doesn't have the auto-fix part: https://www.drupal.org/project/linkchecker

My notes on the need for a good external / internal combo for checking for broken links might be relevant here.


Has a different purpose but code may be super useful: https://www.drupal.org/project/web_page_archive


See https://social.coop/@GuerillaOntologist/102617272946795704 for motivation.  Should at least suggest a browser plugin.
