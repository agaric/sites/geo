# Drupal 7 to Drupal 8 Field Mappings

Almost all the migration has already been done.  These are some leftovers, which were fields that did not have a Drutopia counterpart for the most part and so did not exist at the time of initial content migration:


# Content: Article

| Source | Target |
| ------ | ------ |
| `field_subtitle` |`field_subtitle` |
| `field_publication_date` | `field_publication_date` |

# Vocabulary: Department

As documented, *Review* should become a term in the `article_type` vocabulary.

In addition, *Article* should also become a term in the `article_type` vocabulary (ideally with a name change to *GEO Original*).  We undid this because many of these items did *not* have citations and at least some were not GEO Originals, see https://gitlab.com/agaric/sites/geo/issues/10#note_199134134

This is tricky, but ideally any story *without* a citation would get an `article_type` of *Repost* or similar.
