## Uninstall

### The following will be uninstalled automatically on deploy to D9-latest (now merged into master branch)

- Color
- Quick Edit
- Seven

### The following can be removed manually after the D10 upgrade is complete

- Stable (uninstall and remove from composer.json)
- Geo upgrade (this one is actually now used during the upgrade, and it may not hurt to keep it around, though it can be uninstalled at the end)

### Migration related modules

Modules related to the migration from D7 to D8 are still enabled, they can probably be disabled, but this can be done after the upgrade is finished.

## Drupal 9.3.3 -> Drupal 9 latest

1. Add the following to the vault member:
```
post_deploy:
        - command: "then stable9"
          action: drush
```
2. Deploy master branch to site
3. Remove the post_deploy command from the vault member

## Drupal 9 latest -> Drupal 10

1. Deploy drupal10-rebased branch to site
2. Edit media items and apply cropping where needed (likely the hero blocks in header and footer will be tiny)
3. Uninstall Stable theme and Geo Upgrade custom module (remember to commit this change)
4. After finally deployed to live, remove the root composer dependency on honeypot (this was there because honeypot needed a constraint in order to apply database updates correctly)
