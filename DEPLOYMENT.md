Set up 

Make sure all your code is pushed, including an updated composer.lock file if you've made any changes to modules, themes, or libraries brought in that way.

```
cd ~/Projects/drutopia-platform/drutopia_host/hosting_private/
ahoy git-pull-all
ahoy deploy-build news
ahoy deploy-site geo_test
```

When you're absolutely certain and ready to do a force override quickly:

```
ahoy deploy-build news && ahoy deploy-site-force geo_test
```

And same for `_live` of course.
