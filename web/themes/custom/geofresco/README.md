# GEO Fresco

A subtheme of Octavia, which is a subtheme of the base theme Bulma for Drupal.
It has Bulma CSS framework, Sass, and Font Awesome built in.

@TODO radical new plan for making this theme, and maybe many Drutopia
themes like it, way more sane and acceptable:

Everything using the Bulma flexbox approach.

<div class="columns">
  <div class="column">
    Logo
  </div>
  <div class="column">
    Word-name
  </div>
  <div class="column">
    <div class="columns">
      <div class="column">
        <div class="columns"
          <div class="column">
            search
          </div>
          <div class="column">
            user menu
          </div>
        </div>
      </div>
      <div class="column">
        main menu
      </div>
    </div>
  </div>
</div>


## Getting Started

### Browser Support
Autoprefixer & Babel is set to support:

* IE >= 9
* Last 3 versions of modern browsers.

These can be updated at any time within the `package.json`.

### Run the following commands from the theme directory
If you haven't yet, install nvm:
https://github.com/creationix/nvm

#### Use the right version of node with:

`nvm use`

_This command will look at your `.nvmrc` file and use the version node.js specified in it. This ensures all developers use the same version of node for consistency._

#### If that version of node isn't installed, install it with:

`nvm install`

#### Install npm dependencies with

`npm install`

_This command looks at `package.json` and installs all the npm dependencies specified in it.  Some of the dependencies include gulp, autoprefixer, gulp-sass and others._

#### Compile CSS

  ```
  ./css
  ```
