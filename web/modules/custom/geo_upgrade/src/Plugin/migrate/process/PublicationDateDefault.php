<?php


namespace Drupal\geo_upgrade\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "publication_date_default",
 *   handle_multiples = TRUE
 * )
 */
class PublicationDateDefault extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $created_date = $row->getSourceProperty('created');
    if (!isset($value) || empty($value)) {
      $value = [0 => ['value' => date('Y-m-d\TH:i:s', $created_date)]];
    }
    return $value;
  }
}
