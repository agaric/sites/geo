<?php

namespace Drupal\geo_upgrade\Plugin\migrate\process;

use Drupal\migrate_plus\Plugin\migrate\process\SkipOnValue;

/**
 * If the source evaluates to a configured value, skip processing or whole row.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_on_match"
 * )
 *  Similar to SkipOnValue but this just compare if part of the string match.
 *
 * @see \Drupal\migrate_plus\Plugin\migrate\process\SkipOnValue
 */
class SkipOnMatch extends  SkipOnValue {

  /**
   * Compare values to see if they are equal.
   *
   * @param mixed $value
   *   Actual value.
   * @param mixed $skipValue
   *   Value to compare against.
   * @param bool $equal
   *   Compare as equal or not equal.
   *
   * @return bool
   *   True if the compare successfully, FALSE otherwise.
   */
  protected function compareValue($value, $skipValue, $equal = TRUE) {
    if ($equal) {
      return strpos((string) $value, (string) $skipValue) !== FALSE;
    }

    return  strpos((string) $value, (string) $skipValue) === FALSE;
  }

}
